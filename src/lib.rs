use std::{collections::HashMap, time::Duration};

use dbus::arg::{RefArg, Variant};
use ofdom::OrgFreedesktopDBusObjectManager;

use crate::goa::OrgGnomeOnlineAccountsOAuth2Based;

mod goa;
mod ofdom;

#[derive(Debug, Default)]
pub struct Account {
    pub id: String,
    pub provider_icon: String,
    pub provider_name: String,
    pub identity: String,
    pub oauth2: bool,
    pub mail: Option<MailAccount>,
    pub calendar: Option<CalendarAccount>,
}

impl Account {
    pub fn new(path: String) -> Self {
        Self {
            id: path,
            ..Default::default()
        }
    }
}

#[derive(Debug, Default)]
pub struct MailAccount {
    pub email: String,
    pub imap_host: String,
    pub smtp_user_name: String,
    pub smtp_host: String,
}

#[derive(Debug, Default)]
pub struct CalendarAccount {
    pub uri: String,
}

pub fn get_accounts() -> Vec<Account> {
    let conn = dbus::blocking::Connection::new_session().unwrap();
    let proxy = conn.with_proxy(
        "org.gnome.OnlineAccounts",
        "/org/gnome/OnlineAccounts",
        Duration::from_secs(5),
    );

    let mut accounts = Vec::new();

    if let Ok(objects) = proxy.get_managed_objects() {
        for object in objects {
            let path = object.0.to_string();

            if object.1.is_empty() || object.1.len() == 1 {
                continue;
            }

            let mut account = Account::new(path);

            let content = object.1;
            for (k, v) in content {
                // println!("{} => {:?}\n", k, v);
                match k.as_str() {
                    "org.gnome.OnlineAccounts.Account" => {
                        account.identity = get_field(&v, "PresentationIdentity");
                        account.provider_icon = get_field(&v, "ProviderIcon");
                        account.provider_name = get_field(&v, "ProviderName");
                    }
                    "org.gnome.OnlineAccounts.Mail" => {
                        account.mail = Some(MailAccount {
                            email: get_field(&v, "EmailAddress"),
                            imap_host: get_field(&v, "ImapHost"),
                            smtp_host: get_field(&v, "SmtpHost"),
                            smtp_user_name: get_field(&v, "SmtpUserName"),
                        });
                        account.oauth2 = get_field_bool(&v, "SmtpAuthXoauth2");
                    }
                    "org.gnome.OnlineAccounts.Calendar" => {
                        account.calendar = Some(CalendarAccount {
                            uri: get_field(&v, "Uri"),
                        })
                    }
                    _ => {
                        // ignore
                    }
                }
            }

            accounts.push(account);
        }
    }

    accounts
}

fn get_field(v: &HashMap<String, Variant<Box<dyn RefArg>>>, k: &str) -> String {
    v.get(k)
        .and_then(|s| s.as_str())
        .unwrap_or_default()
        .to_string()
}

fn get_field_bool(v: &HashMap<String, Variant<Box<dyn RefArg>>>, k: &str) -> bool {
    let value = v.get(k).unwrap();
    let bool_value = value
        .0
        .as_any()
        .downcast_ref::<bool>()
        .cloned()
        .unwrap_or_default();
    bool_value
}

pub fn get_token(account_id: &str) -> Option<String> {
    let conn = dbus::blocking::Connection::new_session().unwrap();
    let proxy = conn.with_proxy(
        "org.gnome.OnlineAccounts",
        account_id,
        Duration::from_secs(5),
    );

    if let Ok((token, _)) = proxy.get_access_token() {
        Some(token)
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use crate::{get_accounts, get_token};

    #[test]
    pub fn local_test() {
        let accounts = get_accounts();
        println!("{:#?}", accounts);
        for account in &accounts {
            if account.oauth2 {
                let token = get_token(&account.id);
                println!("token: {:?}", token);
            }
        }
    }
}
