#![allow(clippy::all)]
// This code was autogenerated with `dbus-codegen-rust `, see https://github.com/diwic/dbus-rs
use dbus as dbus;
#[allow(unused_imports)]
use dbus::arg;
use dbus::blocking;

pub trait OrgGnomeOnlineAccountsAccount {
    fn remove(&self) -> Result<(), dbus::Error>;
    fn ensure_credentials(&self) -> Result<i32, dbus::Error>;
    fn provider_type(&self) -> Result<String, dbus::Error>;
    fn provider_name(&self) -> Result<String, dbus::Error>;
    fn provider_icon(&self) -> Result<String, dbus::Error>;
    fn id(&self) -> Result<String, dbus::Error>;
    fn is_locked(&self) -> Result<bool, dbus::Error>;
    fn is_temporary(&self) -> Result<bool, dbus::Error>;
    fn set_is_temporary(&self, value: bool) -> Result<(), dbus::Error>;
    fn attention_needed(&self) -> Result<bool, dbus::Error>;
    fn identity(&self) -> Result<String, dbus::Error>;
    fn presentation_identity(&self) -> Result<String, dbus::Error>;
    fn mail_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_mail_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn calendar_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_calendar_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn contacts_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_contacts_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn chat_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_chat_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    // #[deprecated(note = "true")]
    fn documents_disabled(&self) -> Result<bool, dbus::Error>;
    // #[deprecated(note = "true")]
    fn set_documents_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    // #[deprecated(note = "true")]
    fn maps_disabled(&self) -> Result<bool, dbus::Error>;
    // #[deprecated(note = "true")]
    fn set_maps_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn music_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_music_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn printers_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_printers_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn photos_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_photos_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn files_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_files_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    fn ticketing_disabled(&self) -> Result<bool, dbus::Error>;
    fn set_ticketing_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    // #[deprecated(note = "true")]
    fn todo_disabled(&self) -> Result<bool, dbus::Error>;
    // #[deprecated(note = "true")]
    fn set_todo_disabled(&self, value: bool) -> Result<(), dbus::Error>;
    // #[deprecated(note = "true")]
    fn read_later_disabled(&self) -> Result<bool, dbus::Error>;
    // #[deprecated(note = "true")]
    fn set_read_later_disabled(&self, value: bool) -> Result<(), dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsAccount for blocking::Proxy<'a, C> {

    fn remove(&self) -> Result<(), dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.Account", "Remove", ())
    }

    fn ensure_credentials(&self) -> Result<i32, dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.Account", "EnsureCredentials", ())
            .and_then(|r: (i32, )| Ok(r.0, ))
    }

    fn provider_type(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "ProviderType")
    }

    fn provider_name(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "ProviderName")
    }

    fn provider_icon(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "ProviderIcon")
    }

    fn id(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "Id")
    }

    fn is_locked(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "IsLocked")
    }

    fn is_temporary(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "IsTemporary")
    }

    fn attention_needed(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "AttentionNeeded")
    }

    fn identity(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "Identity")
    }

    fn presentation_identity(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "PresentationIdentity")
    }

    fn mail_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "MailDisabled")
    }

    fn calendar_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "CalendarDisabled")
    }

    fn contacts_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "ContactsDisabled")
    }

    fn chat_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "ChatDisabled")
    }

    // #[deprecated(note = "true")]
    fn documents_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "DocumentsDisabled")
    }

    // #[deprecated(note = "true")]
    fn maps_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "MapsDisabled")
    }

    fn music_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "MusicDisabled")
    }

    fn printers_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "PrintersDisabled")
    }

    fn photos_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "PhotosDisabled")
    }

    fn files_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "FilesDisabled")
    }

    fn ticketing_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "TicketingDisabled")
    }

    // #[deprecated(note = "true")]
    fn todo_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "TodoDisabled")
    }

    // #[deprecated(note = "true")]
    fn read_later_disabled(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Account", "ReadLaterDisabled")
    }

    fn set_is_temporary(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "IsTemporary", value)
    }

    fn set_mail_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "MailDisabled", value)
    }

    fn set_calendar_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "CalendarDisabled", value)
    }

    fn set_contacts_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "ContactsDisabled", value)
    }

    fn set_chat_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "ChatDisabled", value)
    }

    // #[deprecated(note = "true")]
    fn set_documents_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "DocumentsDisabled", value)
    }

    // #[deprecated(note = "true")]
    fn set_maps_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "MapsDisabled", value)
    }

    fn set_music_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "MusicDisabled", value)
    }

    fn set_printers_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "PrintersDisabled", value)
    }

    fn set_photos_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "PhotosDisabled", value)
    }

    fn set_files_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "FilesDisabled", value)
    }

    fn set_ticketing_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "TicketingDisabled", value)
    }

    // #[deprecated(note = "true")]
    fn set_todo_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "TodoDisabled", value)
    }

    // #[deprecated(note = "true")]
    fn set_read_later_disabled(&self, value: bool) -> Result<(), dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::set(&self, "org.gnome.OnlineAccounts.Account", "ReadLaterDisabled", value)
    }
}

pub trait OrgGnomeOnlineAccountsOAuth2Based {
    fn get_access_token(&self) -> Result<(String, i32), dbus::Error>;
    fn client_id(&self) -> Result<String, dbus::Error>;
    fn client_secret(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsOAuth2Based for blocking::Proxy<'a, C> {

    fn get_access_token(&self) -> Result<(String, i32), dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.OAuth2Based", "GetAccessToken", ())
    }

    fn client_id(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.OAuth2Based", "ClientId")
    }

    fn client_secret(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.OAuth2Based", "ClientSecret")
    }
}

pub trait OrgGnomeOnlineAccountsOAuthBased {
    fn get_access_token(&self) -> Result<(String, String, i32), dbus::Error>;
    fn consumer_key(&self) -> Result<String, dbus::Error>;
    fn consumer_secret(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsOAuthBased for blocking::Proxy<'a, C> {

    fn get_access_token(&self) -> Result<(String, String, i32), dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.OAuthBased", "GetAccessToken", ())
    }

    fn consumer_key(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.OAuthBased", "ConsumerKey")
    }

    fn consumer_secret(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.OAuthBased", "ConsumerSecret")
    }
}

pub trait OrgGnomeOnlineAccountsPasswordBased {
    fn get_password(&self, id: &str) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsPasswordBased for blocking::Proxy<'a, C> {

    fn get_password(&self, id: &str) -> Result<String, dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.PasswordBased", "GetPassword", (id, ))
            .and_then(|r: (String, )| Ok(r.0, ))
    }
}

pub trait OrgGnomeOnlineAccountsManager {
    fn add_account(&self, provider: &str, identity: &str, presentation_identity: &str, credentials: arg::PropMap, details: ::std::collections::HashMap<&str, &str>) -> Result<dbus::Path<'static>, dbus::Error>;
    fn is_supported_provider(&self, provider_type: &str) -> Result<bool, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsManager for blocking::Proxy<'a, C> {

    fn add_account(&self, provider: &str, identity: &str, presentation_identity: &str, credentials: arg::PropMap, details: ::std::collections::HashMap<&str, &str>) -> Result<dbus::Path<'static>, dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.Manager", "AddAccount", (provider, identity, presentation_identity, credentials, details, ))
            .and_then(|r: (dbus::Path<'static>, )| Ok(r.0, ))
    }

    fn is_supported_provider(&self, provider_type: &str) -> Result<bool, dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.Manager", "IsSupportedProvider", (provider_type, ))
            .and_then(|r: (bool, )| Ok(r.0, ))
    }
}

pub trait OrgGnomeOnlineAccountsMail {
    fn email_address(&self) -> Result<String, dbus::Error>;
    fn name(&self) -> Result<String, dbus::Error>;
    fn imap_supported(&self) -> Result<bool, dbus::Error>;
    fn imap_accept_ssl_errors(&self) -> Result<bool, dbus::Error>;
    fn imap_host(&self) -> Result<String, dbus::Error>;
    fn imap_use_ssl(&self) -> Result<bool, dbus::Error>;
    fn imap_use_tls(&self) -> Result<bool, dbus::Error>;
    fn imap_user_name(&self) -> Result<String, dbus::Error>;
    fn smtp_supported(&self) -> Result<bool, dbus::Error>;
    fn smtp_accept_ssl_errors(&self) -> Result<bool, dbus::Error>;
    fn smtp_host(&self) -> Result<String, dbus::Error>;
    fn smtp_use_auth(&self) -> Result<bool, dbus::Error>;
    fn smtp_auth_login(&self) -> Result<bool, dbus::Error>;
    fn smtp_auth_plain(&self) -> Result<bool, dbus::Error>;
    fn smtp_auth_xoauth2(&self) -> Result<bool, dbus::Error>;
    fn smtp_use_ssl(&self) -> Result<bool, dbus::Error>;
    fn smtp_use_tls(&self) -> Result<bool, dbus::Error>;
    fn smtp_user_name(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsMail for blocking::Proxy<'a, C> {

    fn email_address(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "EmailAddress")
    }

    fn name(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "Name")
    }

    fn imap_supported(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "ImapSupported")
    }

    fn imap_accept_ssl_errors(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "ImapAcceptSslErrors")
    }

    fn imap_host(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "ImapHost")
    }

    fn imap_use_ssl(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "ImapUseSsl")
    }

    fn imap_use_tls(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "ImapUseTls")
    }

    fn imap_user_name(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "ImapUserName")
    }

    fn smtp_supported(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpSupported")
    }

    fn smtp_accept_ssl_errors(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpAcceptSslErrors")
    }

    fn smtp_host(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpHost")
    }

    fn smtp_use_auth(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpUseAuth")
    }

    fn smtp_auth_login(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpAuthLogin")
    }

    fn smtp_auth_plain(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpAuthPlain")
    }

    fn smtp_auth_xoauth2(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpAuthXoauth2")
    }

    fn smtp_use_ssl(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpUseSsl")
    }

    fn smtp_use_tls(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpUseTls")
    }

    fn smtp_user_name(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Mail", "SmtpUserName")
    }
}

pub trait OrgGnomeOnlineAccountsCalendar {
    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error>;
    fn uri(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsCalendar for blocking::Proxy<'a, C> {

    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Calendar", "AcceptSslErrors")
    }

    fn uri(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Calendar", "Uri")
    }
}

pub trait OrgGnomeOnlineAccountsContacts {
    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error>;
    fn uri(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsContacts for blocking::Proxy<'a, C> {

    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Contacts", "AcceptSslErrors")
    }

    fn uri(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Contacts", "Uri")
    }
}

pub trait OrgGnomeOnlineAccountsChat {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsChat for blocking::Proxy<'a, C> {
}

// #[deprecated(note = "true")]
pub trait OrgGnomeOnlineAccountsDocuments {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsDocuments for blocking::Proxy<'a, C> {
}

// #[deprecated(note = "true")]
pub trait OrgGnomeOnlineAccountsMaps {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsMaps for blocking::Proxy<'a, C> {
}

pub trait OrgGnomeOnlineAccountsMusic {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsMusic for blocking::Proxy<'a, C> {
}

pub trait OrgGnomeOnlineAccountsPhotos {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsPhotos for blocking::Proxy<'a, C> {
}

pub trait OrgGnomeOnlineAccountsFiles {
    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error>;
    fn uri(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsFiles for blocking::Proxy<'a, C> {

    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Files", "AcceptSslErrors")
    }

    fn uri(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Files", "Uri")
    }
}

pub trait OrgGnomeOnlineAccountsExchange {
    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error>;
    fn host(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsExchange for blocking::Proxy<'a, C> {

    fn accept_ssl_errors(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Exchange", "AcceptSslErrors")
    }

    fn host(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Exchange", "Host")
    }
}

pub trait OrgGnomeOnlineAccountsMediaServer {
    fn dlna_supported(&self) -> Result<bool, dbus::Error>;
    fn udn(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsMediaServer for blocking::Proxy<'a, C> {

    fn dlna_supported(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.MediaServer", "DlnaSupported")
    }

    fn udn(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.MediaServer", "Udn")
    }
}

pub trait OrgGnomeOnlineAccountsTicketing {
    fn get_ticket(&self) -> Result<(), dbus::Error>;
    fn details(&self) -> Result<::std::collections::HashMap<String, String>, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsTicketing for blocking::Proxy<'a, C> {

    fn get_ticket(&self) -> Result<(), dbus::Error> {
        self.method_call("org.gnome.OnlineAccounts.Ticketing", "GetTicket", ())
    }

    fn details(&self) -> Result<::std::collections::HashMap<String, String>, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.gnome.OnlineAccounts.Ticketing", "Details")
    }
}

// #[deprecated(note = "true")]
pub trait OrgGnomeOnlineAccountsTodo {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsTodo for blocking::Proxy<'a, C> {
}

// #[deprecated(note = "true")]
pub trait OrgGnomeOnlineAccountsReadLater {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsReadLater for blocking::Proxy<'a, C> {
}

pub trait OrgGnomeOnlineAccountsPrinters {
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgGnomeOnlineAccountsPrinters for blocking::Proxy<'a, C> {
}
